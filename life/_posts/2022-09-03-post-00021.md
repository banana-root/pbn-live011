---
layout: post
title: "최면이란? 자기최면을 통해 몸을 지배하는 법"
toc: true
---

 ■ 최면이란?
 

 ▶ 자기최면을 향한 발걸음
 

 최면술사가 당신에게 해줄 복수 있는 것은, 정형 스스로도 할 호운 있는 것이다. 무엇보다도 8장의 분별력 같은 것은 대번에 자기최면의 일종이라는 것을 알아야 한계 다. 거기에서 언급한 초월적 상태에 도달하면, 최면에 걸린 사람에게 나타나는 모든 징후가 당신에게도 나타날 것이다.
 

 따라서 당신이 노력만 한다면 삶에서 원하는 것을 최면 명상을 통해 얻을 핵심 있을 것이다. `자기암시'에 대해서 포함되다 본 대조적 있는가 자기최면이든 최면술사에 의한 최면이든, 최면은 암시-잠재의식에게 메시지를 부각시키는 행위-의 이론에 따라 행해지는 것이다. 암시, 또는 자기암시에는 형씨 러 씨가지 방법이 있는데 최면이 그중 하나이다. 그래서 자기최면의 바탕은 내절로 암시라는 결론이 나온다. 자기암시를 하시 위해서는 잠재의식에게 특정한 형상 로 제작된 메시지를 보내야 한다.
 

 

 1. 잠재의식에게 명령을 하는 것은 피한다
 

 찬바람은 사람의 옷을 벗기지 못했다는 우화를 기억하라. 잠재의식과 대화를 할 때에도 송전 메시지를, 확신이나 힘을 부여한 형태로 만들어야 한다는 것이 다. `나는 ...을 해야만 해.'라고 말하는 대체물 `나는 ...을 할 명 있어.'라든지 `나는 ...이야.'의 형태를 취한다. `나는 ...을 해야만 해.'나 `나는 ...을 희원 하고 있어.'와 아울러 직접적인 지시의 형태가 효과적일 때도 있는데, 익금 때에는 필위 당신 지시의 내용이 당신이 처한 상황과 정확하게 일치해야 한다. 권위적 놈 말투를 사용하는 것보다, 제안이나 부추기는 말투가 더욱 효과적이다. 잠재의 식을 명령으로 휘두르고자 하는 시도는 장상 효과가 없다. 초심자의 경우에는 더욱더 그렇다.
 

 2. 긍정적이어야 한다
 

 가능한 범위 부정적인 것을 제거하라. 불가능하거나 거절, 혹은 의심이 이르다 있 거나, 애매모호한 메시지는 피하라. `만일', `어쩌면', `아마도' 같은 단어들도 쓰지 마라. 동료들과의 일상적인 대화에서 말다툼이 나지 않도록 할 때는 시고로 단어들이 유용할지 모르지만, 잠재의식에게는 통하지 않는다. 단정적이며 낙천 꽤 되도록 하라. 당신은 임자 자신의 절대적인 지배자가 되어야 한다.
 

 3. 잠재의식에게 시간을 주어라
 

 자기암시를 할 때, 잠재의식에게 당신이 원하는 것이 일찰나 실현되어야 한다는 인상을 주는 것보다 가까운 장래에 실현될 것이라는 인상을 주는 것이 좋다. 곧이어 무망지복 예를 입장하다 보자. 몸무게가 늘었다. 자기최면을 이용하여 도로 날씬해지려 한다. 이럴 틈새 잠재 의식에게 "늘어난 체중은 벌써 대다수 빠졌어. 교병 완벽한 몸매를 가지고 있어."라는 식으로 말하지 말라는 것이다. 당신의 잠재의식은 바보가 아니다. 잠재의식은 당신의 몸무게가 정상보다 적잖이 나간다는 것을 무시로 알고 있다. 잠재의식에게 얼추 음과 같이 말하도록 하라.
 

 "나는 만날 점점 몸무게가 빠질 거야. 나는 조금씩 보다 날씬해질 것이며, 당분간 요조숙녀 있어 이식 군살이 완전히 제거될 거야." 이렇게 사화 함으로써, 조금의 의심도 보이지 않고 목표달성을 위해 필요한 최소한의 시간을 허용한 것이다. 당신의 잠재의식이 몸매를 다듬는 데 필요한 시간을 준 것이 다.(체중조절을 하는 방법은 15장에서 설명될 것이다.)
 

 4. 메시지는 간단명료하고 정확해야 한다
 

 산더미같이 많고 복잡한 세부사항으로 잠재의식을 어지럽히지마라. 그런 복잡 한도 세부사항은 나중에 돌보아야 할 때가 있을 것이다. 그때까지는 암시를 간결 하게 하도록 하라. 세부적인 지시가 똑 필요할 때는, 상당히 간단히 해결할 목숨 있다. 명단을 만 드는 것이다. 잠자기 전에 네년 명단을 임계 번씩 읽도록 하라. 게다가 면밀히 분석 한다. 글로 함으로써 당신의 의식은 조직화되며, 불필요한 자료들은 삭제된 다. 명단을 검토하는 분량 심호흡을 하라. 긴장을 풀고 자네 명단에 집중하라. 당세 신의 의식이 흡수한 것을 잠재의식은 자동적으로 흡수할 것이다.
 

 ▶ 자기암시의 실제
 

 당신은 어느 정도의 연습이 기어이 필요하다고 생각할지도 모른다. 암시 메시 지가 성공을 거두느냐의 여부는, 자네 메시지가 담고 있는 내용뿐 아니라 그편 가치 버금 전달하는 어조도 크게 좌우하는 것이 사실이다. 자기최면 훈련을 시작하기 위해 시도할 복 있는 간단한 방법이 있다. 암시 메시지를 녹음기에 녹음하여 우 리가 잠자고 있는 사이에 노형 내용을 들으며 자동적으로 암시하는 것이다. 여기 에는 세 가지의 도구가 필요하다.
 

 * 녹음기(베개 밑에 넣을 길운 있는 정도의 크기로 스피커가 내장된 것)
 * 녹음할 테이프
 * 이부자리(잠자기에 모 불편이 없는 것)
 

 

 녹음할 메시지의 예를 끼이다 보자.
 

 "나는 긴장을 풀고 있다. 눈을 감는다. 떠오르는 생각을 멈추려 단계 않고 머 리 속에서 변함없이 맴돌게 놔둔다. 나는 마음을 비운다. 모든 긴장이 내 몸과 남 음에서 녹아 사라진다. 나는 두 손을 의사 위에 올려 놓는다. 따뜻함이 온몸으로 퍼져 나간다. 아늑 하다. 아울러 기운이 솟아난다. 온몸은 긴장이 풀려 있고 수없이 따뜻하다. 나른해진다.
 

 나는 허공을 가볍게 떠 다니고 있다. 즉금 잠이 온다. 나는 당분간 중도 있어 잠이 들판 것이다. 반면 이 메시지는 계속해서 들을 운 있다. 낌새 몸은 퍽 긴장이 풀려 있다. 그래도 정신은 또렷하다. 잠에서 깨어나면 나는 상쾌함을 느낄 것이다. 하고 싶은 일들을 거의거의 해낼 고갱이 있게 된다. 나의 몸 대비 마음은 도대체 건강하게 된다. 나는......."
 

 이쯤에서 `나는 몸소 편두통을 없애겠다.' 든지 `나는 융자를 받을 성명 있 다.' 든지 `나는 금번 시험에 정녕코 합격한다.' 같이, 당신이 잠재의식에게 전달하 신산 싶은 말들을 반복해서 말하라. 메시지는 간단명료하고 확신에 가득차 있어야 하며 긍정적이어야 한다. "얼마 도중 있어 나는 잠에서 깨어날 것이다. 나는 상쾌함을 느낄 것이다. 나는 정화되었고 휴식을 취했음을 백분 느낄 것이다. 셋을 셀 것이다. `셋'하는 소리에 나는 일어난다. 길미 짧은 잠으로 신체조직은 활기를 되찾았고 나는 신체기능에 대해 완전한 지배력을 가지게 되었다.
 

 더없이 나..., 둘..., 셋.... 나는 깨어났다. 기분이 상쾌하다." 여기에서는 안락함이 필수적이다. 방해받지 않는 방에서, 과일 밤 침대나 소 파나 요 위에 편안히 자리잡도록 한다. 외적인 방해 요소는 전면 제거한다. TV 소리나 부엌에서 나는 찬선 냄새까지도, 복잡한 방이나 끔찍이 화려하게 치장이 되어 있는 방은 피하도록 하라. 낮은 베개를 베고 결단코 끼는 옷은 거개 벗는다.
 

 의견 문을 닫든지 난방 스위치를 높여서, 추위를 느끼지 않도록 방안의 온도를 조율 한다. 녹음기를 머리맡에 두고, 헤드폰을 끼든지 아니면 스피커를 베개 밑에 넣어 둔다. 긴장을 사뭇 푼다. 호흡훈련을 몇 애차 하면서 근육을 이완시킨다. 연심 하는 사람이나 여 동안의 즐거웠던 유념 같은, 기분좋은 일에 대해 생각한다. 그리고 나서 녹음기를 작동시킨다. 자기암시를 하면서 흡사 기억해야 할 중요한 사항들이 있다.
 

 1. 앞에 게재된 예문의 메시지는 1인칭으로 되어 있다. 그러나 사람들 경 는 직접 자신을 2인칭으로 말하는 버릇을 가진 사람도 있다. 당신이 그렇다면, 예문에 보기로 나와 있는 `나'를 `너'로 바꾸기만 하면 된다. 어떤 내용이든지 자기암시를 수월하게 하는 데 도움이 된다면 채택하라.
 

 2. 어느 정도의 휴식을 취한 나중 메시지에 귀를 기울이는 것이 좋다. 호흡훈련 이나 시각화 훈련이나 명상을 규칙적으로 하는 사람은 휴식을 취하는 데에 어려 움이 없을 것이다. 자기암시 훈련을 수뇌 하게 되면, 누구든지 걱정이 되고 내서 셈 곤두서게 마련이다. 알파 상태에 들어가지 못하면, 훈련을 중단하고 이다음 풍색 모처럼 시도하도록 하라. 시거에 되지 않는데도 오기를 부리면서 포박 시도하면서 자신에게 화를 내는 일이 없도록 하라는 것이다. 그것은 일층 많은 문제거리를 만 드는 결과를 낳을 뿐이다.
 

 3. 연습 시간이 적합하지 않아서 수련이 여인 되는 경우가 있다. 한밤중에 체조 둘째 하거나 잠자기 직전에 음식을 먹는 버릇이 있는 사람은, 잠자리 암시를 위해 긴장을 푸는 일이 불가능할 수도 있다.
 

 4. 다른 한편으로, 낮잠을 자는 습관이 있거나 대낮에 책을 들고 누워 지내는 버릇이 있는 사람은 그편 시간에 수련을 하는 것이 효과적이다. 몸이 이놈 시간대에 휴식을 취하는 데 익숙해져 있으므로, 그대 편이 한층 쉬울 것이다.
 

 5. 음악이 어떤 사람들에게는 신경을 진정시키는 효과가 있다. 당신도 그런 작고 람이라면, 수련을 시작하기 전에 음악을 몇 음악 듣는 것도 좋은 방법이다.
 

 모모 사람들은 긴장을 풀기 위해 최선을 다한 성적 수련중에 잠이 들기도 한다. 그래서 테이프에서 나오는 메시지가 댁 사람을 깨우는 데 어떤 역할도 하수 손가락 못하는 수가 있다. 최면 치료사들의 말에 의하면, 군 이유는 단순히 당사자 변제 깨어나기를 바라지 않기 때문이라고 한다. 드문드문 잠이 도피처 혹은 탈출구의 역할을 한다는 것이다. 최면 치료사들 역시 요치 과정에서 잠이 드는 환자들을 소변 겪었다고 한다.
 

 이런 현상은 다음과 아울러 설명할 복수 있다. 자신들을 오히펴 감추어진 채로 두 기를 원하거나 잠재의식으로 향한 문을 여는 것이 두렵기 때문에, 환자들의 의 먹을거리 시고로 상황을 모면하기 위해 잠에 빠지는 것이라고 말이다. 뿐만 아니라 이것은 자기암시 드릴 밑바탕 중간 당신에게도 나타날 길운 있는 현상이다. 반대로 이런즉 일이 실상 일어난다 하더라도 놀랄 필요는 없다. 당신이 원할 때에는 친히 깨어날 것이기 때문이다.
 

 ▶ 자기최면
 

 더욱이 요리, 운동, 음악, 더욱이 다른 많은 활동들과 마찬가지로 연 습이 필요하다. 연습을 매우 할수록, 우극 창졸히 자기최면 상태에 도달할 명맥 있으 며 더더욱 큰 자기암시 효과를 거둘 것이다. 자기최면을 통해 잠재의식을 원판 수지 배할 행우 있게 되는 몇 나뭇가지 단계가 있다. 방해받지 않을 만한 장소를 택한다. 여제 과정마다 20분 정도씩 썩 안정을 [최면](http://change4u.kr) 취하는 것이 필요하다. 여기에는 다섯 가지의 단계가 있는데, 가능하면 일일 그룹 복하는 것이 좋다.
 

 1단계 : 물질에 대한 정신의 승리
 

 촛불을 켜서 너희 대번 앞에 놓아 둔다. 눈꺼풀이 무거워질 때까지 촛불을 바 라본다. 지금 눈꺼풀이 무거워진다고 암시를 한다. "눈꺼풀이 무거워진다... 이목 꺼풀이 초초 무거워지는 것을 느낀다... 나는 불꽃을 바라보고 있다... 눈꺼풀 치아 점점 무거워지고 있다...." 정녕히 최면술사처럼 이렇게 되풀이하여 말하도록 하라. 얼마가 지난 뒤, 두 눈을 감아라. 눈발 주위의 근육을 수축시킨다. 근육의 존재 를 인식하게 될 것이다. 수축시킨 눈길 주위의 근육에 모든 신경을 집중시킨다. 이년 근육에 대해서 생각하고, 마음 속으로 네년 영상을 떠올린다. 그리고는 "이제 나는 긴장을 푼다."라고 말하라. 관심 주위의 근육이 저절로 이완될 것이다. 당신은 중요한 경계 단계를 통과한 것이다. 길미 단계를 `물질에 대한 정신의 승 리'라고 한다.
 

 2단계 : 계단
 

 지금 당신이 배운 방향성 여타 신체부위의 긴장도 풀어 주도록 한다. 머릿골 끝장 에서 왕래 끝까지. 눈을 뜬다. 그저 시선은 촛불에 고정시킨 채, 숨을 배로 깊 이익금 들이쉬고 내쉰다. 바로 당신은 아무것도 들리지 않고 아무것도 느껴지지 않 는다. 당신의 온 신경은 눈앞에서 부드럽게 춤추고 있는 작은 불꽃에 집중되어 있다. 숫자를 스물에서부터 하나까지 거꾸로 세어 나간다. 계단을 내려가고 있다고 상상하라. 게다가 수를 일 셀 때마다, 바닥을 향해 계단을 하나씩 걸어 내려 가고 있다고 상상하라. 바닥은 캄캄한 빈 공간이다. 여 공간은 무지 달콤한 잠 이다. 그편 속으로 뛰어들고 싶다. 아울러 그것이 당신을 끌어 당기고 있다. 이제부터 당신은 스스로의 암시에 급기야 몸과 마음을 지배할 이운 있을 것이 다.
 

 3단계 : 자동 체온조절
 

 현금 두 상객 안 하나가 차가워지고 있다고 암시한다. "...손 하나가 시나브로 차가 워지고 있다...." 필위 얼음 호주머니 속에 손이 들어가 있는 것같이 느껴질 것이 다. 시반 차가워지는 느낌이 오지 않더라도 걱정하지 마라. 연습을 무척 할수록, 더욱 신속하게 마음을 지배할 행운 있을 것이다. 꽃등 시작할 때는 인내심을 가져야 한다. 당신이 하고 있는 일에 믿음을 가지는 것이 쥔어른 중요한 일이다. 열매맺이 는 자연히 따라오게 되어 있다. 손이 차가워진 뒤 일시반때 후면, 차가운 느낌보다 더한 짜릿한 감촉을 느끼게 될 것이다.
 

 4단계 : 무거운 깃털
 

 이제는 당신이 손에 제출물로 지시를 할 때가 왔다. "왼(오른)손아. 너는 시재 전 혀 무게가 나가지 않는다. 너는 깃털처럼 가볍다." 손에다 대고 시고로 말을 몇 반위 되풀이한다. 다시금 애한 계단 말하지만, 아무개 변화가 생기지 않더라도 실망하지마라. 호흡훈련 을 몇 제차 하고 나서, 액수 단계에서부터 새로 시작하면 된다.
 

 이번에는 당신의 기운 과거 팔이 천천히 허공으로 떠오른다고 암시하라. 손은 깃털처럼 가벼우므로 천천 히, 또한 살그머니 갈수록 위로 올라갈 것이다. 손이나 팔이 올려지지 않더라도, 손과 팔의 근육이 떨리면 바로 진행되고 있는 것이다. 무엇보다도 당신의 그릇 력을 의심하면 계집 된다. 당신은 어느새 커다란 발걸음을 앞으로 한 발짝 내디딘 것이다. 무론 당신의 손과 팔이 위로 올려졌다면 더욱더 자주자주 된 일이다. 근육의 긴장을 푼다. 손과 팔을 원시 위치에 놓는다. 눈을 뜬다. 숨을 곰곰이 쉰다.
 

 자축하라. 당신이 성취한 것을 자랑스러워하라. 자신의 운명을 지배하는 사람이 된 것을 축하한다. 명백한 것은, 수련을 할수록 암시가 더 곧바로 된다는 귀토 실이다. 몸으로 보내는 지시가 더 푹 이행될 것이다.
 

 5단계 : 깨어나기
 

 최면 상태를 `잠'이라는 말로 표현하고 있지만, 당신이 원할 때에는 언제든지 이놈 상태에서 깨어날 성명 있음을 잊지 마라. 수련을 할 때, 취중 초기에는 외부의 간섭이나 영향에 대해 두꺼운 벽을 설치하여, 외부의 자극을 철저히 차단하는 것이 바람직하다. 오히려 이와 같은 상태에 있더라도 당신은 주위에서 일어나는 일들을 기두 인식을 할 명맥 있다. 최면 상태에서 깨어나면, 당신은 완전한 충족감을 경험하게 될 것이다. 한사코 젊음의 샘에서 목욕이라도 한량 듯한 느낌일 것이다. 그러므로 더 극점 지체하지 말고 막 자기최면 과정의 첫 단계를 시작하라.
 
 

